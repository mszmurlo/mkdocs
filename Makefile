# (c) 2021 - Maurycy Szmurlo (mszmurlo@buguigny.org) - MIT Licence
# 

PUML=/usr/bin/plantuml #java -jar plantuml.jar
DRAWIO=/usr/bin/drawio
PANDOC=/usr/bin/pandoc
MV=/usr/bin/mv
RM=/usr/bin/rm
TOUCH=/usr/bin/touch
SED=/usr/bin/sed

MD_FILES=$(wildcard *.md)
HTML_FILES=$(MD_FILES:.md=.html)
DOCX_FILES=$(MD_FILES:.md=.docx)

DRAWIO_FILES=$(wildcard *.drawio)
DR_SVG_FILES=$(DRAWIO_FILES:.drawio=.dr_svg)
DR_PNG_FILES=$(DRAWIO_FILES:.drawio=.dr_PNG)

PUML_FILES=$(wildcard *.puml)
PU_SVG_FILES=$(PUML_FILES:.puml=.pu_svg)
PU_PNG_FILES=$(PUML_FILES:.puml=.pu_png)

SRC_FILES=$(MD_FILES) $(DRAWIO_FILES) $(PUML_FILES)
SVG_FILES=$(PU_SVG_FILES:.pu_svg=.svg) $(DR_SVG_FILES:.dr_svg=.svg)
PNG_FILES=$(PU_PNG_FILES:.pu_png=.png) $(DR_PNG_FILES:.dr_PNG=.png)

# --- Main targets ----------------------------------------------------------------
all: html
html: $(SVG_FILES) $(HTML_FILES)
docx: $(PNG_FILES) $(DOCX_FILES)
svg: $(SVG_FILES)
png: $(PNG_FILES)
clean: do_clean
# ----------------------------------------------------------------------------------

%.html: %.md style.css
	$(PANDOC) $< --self-contained --toc --css=style.css -N -t html5 -o $@

style.css:
	$(TOUCH) style.css

# I really love *nix!
%.docx: %.md
	$(SED) -E 's/(!\[.*\]\(.*)\.svg\)/\1.png)/g' $< | $(PANDOC) -s --toc -N -t docx -o $@


# --- Make SVG Files out of PlantUML or DrawIO --------------------------------------
%.dr_svg: %.drawio
## draw.io cannot overwrite an existing file ???
	$(RM) -f  $@ 
	$(DRAWIO) -x  --crop -f svg -t -o `basename -s .drawio $<`.dr_svg  $<
	$(TOUCH) *.md

%.pu_svg: %.puml
	$(PUML) $< -tsvg
	$(MV) `basename -s .puml $<`.svg `basename -s .puml $<`.pu_svg
	$(TOUCH) *.md

%.svg: %.pu_svg
	$(MV) $< $@ 

%.svg: %.dr_svg
	$(MV) $< $@
# ----------------------------------------------------------------------------------


# --- Make PNG Files out of PlantUML or DrawIO --------------------------------------
%.dr_png: %.drawio
## draw.io cannot overwrite an existing file ???
	$(RM) -f  $@ 
	$(DRAWIO) -x  --crop -f png -t -o `basename -s .drawio $<`.dr_png  $<
	$(TOUCH) *.md

%.pu_png: %.puml
	$(PUML) $< -tpng
	$(MV) `basename -s .puml $<`.png `basename -s .puml $<`.pu_png
	$(TOUCH) *.md

%.png: %.pu_png
	$(MV) $< $@ 

%.png: %.dr_png
	$(MV) $< $@
# ----------------------------------------------------------------------------------

PHONY: clean

do_clean:
	$(RM) -f *~ *.png *.svg *.html *docx
