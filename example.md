---
title: Documentation generation from Markdown using pandoc
---

`mkdocs` is a `Makefile` that automates the generation of self
contained HTML and Microsoft Word `.docx` files with embedded images
out of a Markdown `.md` file. You'll of course need all the required
dependencies (see below). Images can be produced with *drawio* or with
*plantuml*. The resulting document is generated using *pandoc*.

# Usage
1. Copy files `Makefile` and `style.css` to the directory where you'll
   edit your document
1. Create a `.md` file, as for example `my_doc.md` and add some
   text. You can get some inspiration from the `exemple.md` file and
   of course from the `pandoc` documentation (see below).
1. Add `.drawio` or `.puml` (for PlantUML) files if necessary
1. `make` targets are:
   * `make` or `make html` to create a self contained `my_doc.html`
     file. Images will be embedded as `.svg` files.
   * `make docx` to create a self contained `my_doc.docx` Microsoft
     Word file. Images will be embedded as `.png` files.
   * `make clean` to erase all generated files by previous `make`
     commands
     
HTML styles can be edited in the `style.css` file. I'm not really sure
how to beautify the Word documents...

# Dependencies

* *pandoc* : is a universal document converter from one markup format
  into another. To install, follow the [installation
  instructions](https://pandoc.org/installing.html) or on Linux,
  simply do `sudo apt install pandoc`
* *drawio* : is a free, open source, online and offline diagraming
  tool. The latest version can be downloaded for Windows, Linux and MacOS
  from [Github](https://github.com/jgraph/drawio-desktop/releases/)
* *plantuml* : is a text to image generator dedicated for *not only* UML
  diagrams. It is available for download [from
  here](https://plantuml.com/en/) and can be installed on Linux with
  `sudo apt install platuml`.
* *pygments* : a Python library to highlight source code. It is
  available [here](https://pygments.org/).

# HTML document generation

To generate the HTML file, simply type `make` or `make all`.

## Rendering a diagram created with *drawio*

![](example_drawio.svg)

## Rendering a diagram created  with *plantuml*

![](example_plantuml.svg)

## Example of code highlighting

### Elixir

This code is a Elixir snippet (from
[elixir-lang.org](https://elixir-lang.org/)):

```elixir
current_process = self()

# Spawn an Elixir process (not an operating system one!)
spawn_link(fn ->
  send(current_process, {:msg, "hello world"})
end)

# Block until the message is received
receive do
  {:msg, contents} -> IO.puts(contents)
end
```

### Java

This code is a Java snippet (from [vertx.io](https://vertx.io/)):

```javascript
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```

