
`mkdocs` is a `Makefile` that automates the generation of self
contained HTML and Microsoft Word `.docx` files with embedded images
out of a Markdown `.md` file. You'll of course need all the required
dependencies (see below). Images can be produced with *drawio* or with
*plantuml*. The resulting document is generated using *pandoc*.

# Usage
1. Copy files `Makefile` and `style.css` to the directory where you'll
   edit your document
1. Create a `.md` file, as for example `my_doc.md` and add some
   text. You can get some inspiration from the `exemple.md` file and
   of course from the `pandoc` documentation (see below).
1. Add `.drawio` or `.puml` (for PlantUML) files if necessary
1. `make` targets are:
   * `make` or `make html` to create a self contained `my_doc.html`
     file. Images will be embedded as `.svg` files.
   * `make docx` to create a self contained `my_doc.docx` Microsoft
     Word file. Images will be embedded as `.png` files.
   * `make clean` to erase all generated files by previous `make`
     commands
     
HTML styles can be edited in the `style.css` file. I'm not really sure
how to beautify the Word documents...

# Dependencies

* *pandoc* : is a universal document converter from one markup format
  into another. To install, follow the [installation
  instructions](https://pandoc.org/installing.html) or on Linux,
  simply do `sudo apt install pandoc`
* *drawio* : is a free, open source, online and offline diagraming
  tool. The latest version can be downloaded for Windows, Linux and MacOS
  from [Github](https://github.com/jgraph/drawio-desktop/releases/)
* *plantuml* : is a text to image generator dedicated for *not only* UML
  diagrams. It is available for download [from
  here](https://plantuml.com/en/) and can be installed on Linux with
  `sudo apt install platuml`.
* *pygments* : a Python library to highlight source code. It is
  available [here](https://pygments.org/).
